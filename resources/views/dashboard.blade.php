<x-app-layout>
    <x-slot name="header">
            {{ __('Dashboard') }}
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="text-white">
                <table class="w-full table-auto text-lg">
                    <tr class="text-left border-b border-dotted">
                        <th>Created at</th>
                        <th>Hanzi</th>
                        <th>Pinyin</th>
                        <th>Translation</th>
                        <th>Level</th>
                        <th class="text-right p-3">Actions</th>
                    </tr>

                    @foreach ($pendings as $p)
                        <tr>
                            <td>{{ $p->created_at }}</td>
                            <td>
                                <a href="https://translate.google.com/?sl=zh-CN&tl=en&text={{ $p->hanzi }}&op=translate" class="transition duration-300 text-purple hover:text-green" target="_blank" rel="noopener norefresh">
                                    {{ $p->hanzi }}
                                </a>
                            </td>
                            <td>{{ $p->pinyin }}</td>
                            <td>{{ $p->translation }}</td>
                            <td>{{ $p->level }}</td>
                            <td class="text-right p-3 pt-6">
                                <a href="{{ route('user.word.show.pending', ['pending' => $p->id]) }}" class="transition duration-300 bg-green-700 hover:bg-green-600 p-2 border">Show</a>
                                <a href="{{ route('user.word.delete.pending', ['pending' => $p->id]) }}" class="transition duration-300 bg-red-900 hover:bg-red-700 p-2 border" onclick="return confirm('Do you really want to delete this item?')">Delete</a>
                            </td>
                        </tr>
                    @endforeach
                </table>

                {{ $pendings->links() }}
            </div>
        </div>
    </div>
</x-app-layout>
