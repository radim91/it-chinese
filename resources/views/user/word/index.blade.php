<x-app-layout>
    <x-slot name="header">
        <div class="grid grid-cols-2">
            <div>
                Words
                @if (session('success'))
                    | {{ session('success') }}
                @endif
            </div>

            <div class="text-right">
                <a href="{{ route('user.word.create') }}" class="transition duration-300 text-lg p-3 border hover:bg-gray-700">Create</a>
            </div>

    </x-slot>

    <x-slot name="slot">
        <div class="max-w-7xl mx-auto py-12 px-4 text-white">
            <div class="pt-6 px-4">
                <table class="table-auto w-full">
                    <tr class="text-left">
                        <th>Hanzi</th>
                        <th>Pinyin</th>
                        <th>Translation</th>
                        <th>Level</th>
                        <th>Category</th>
                        <th class="text-right">Actions</th>
                    </tr>

                    @foreach ($words as $w)
                        <tr>
                            <td>{{ $w->hanzi }}</td>
                            <td>{{ $w->pinyin }}</td>
                            <td>{{ $w->translation }}</td>
                            <td>{{ $w->level }}</td>
                            <td>{{ App\Models\Category::where('id', $w->category)->first()->name }}</td>
                            <td class="text-right">
                                <a href="{{ route('user.word.edit', $w->id) }}" class="transition duration-300 text-yellow hover:green">Edit</a>
                                <a href="{{ route('user.word.delete', $w->id) }}" class="transition duration-300 text-red hover:text-purple" onclick="return confirm('Do you really want to remove this word?')">Delete</a>
                            </td>
                        </tr>
                    @endforeach
                </table>

                <div class="bg-transparent pt-12">
                    {{ $words->links() }}
                </div>
            </div>
        </div>
    </x-slot>
</x-app-layout>
