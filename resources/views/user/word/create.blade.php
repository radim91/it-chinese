<x-app-layout>

    <x-slot name="header">
        Create Word
    </x-slot>

    <x-slot name="slot">
        <div class="max-w-7xl mx-auto px-4 py-12 text-white text-lg grid grid-rows-3">
            <form method="POST">
                @csrf

                <div class="px-6">
                    <label>
                        <p class="pb-4">Category</p>
                        <select name="category" class="bg-transparent p-2 border w-1/3">
                            @foreach ($categories as $c)
                                <option value="{{ $c->id }}">{{ $c->name }}</option>
                            @endforeach
                        </select>
                    </label>
                </div>

                <div class="px-6 pt-4">
                    <label>
                        <p class="pb-4">Hanzi</p>
                        <input type="text" name="hanzi" id="hanzi" class="bg-transparent p-2 border w-1/3">
                    </label>
                </div>

                <div class="px-6 pt-4">
                    <label>
                        <p class="pb-4">Pinyin</p>
                        <input type="text" name="pinyin" id="pinyin" class="bg-transparent p-2 border w-1/3">
                    </label>
                </div>

                <div class="px-6 pt-4">
                    <label>
                        <p class="pb-4">Translation</p>
                        <input type="text" name="translation" id="translation" class="bg-transparent p-2 border w-1/3">
                    </label>
                </div>

                <div class="px-6 pt-4">
                    <label>
                        <p class="pb-4">Level</p>
                        <select name="level" class="bg-transparent p-2 border w-1/3">
                            <option value="beginner">Beginner</option>
                            <option value="intermediate">Intermediate</option>
                            <option value="advanced">Advanced</option>
                        </select>
                    </label>
                </div>

                <div class="px-6 pt-8">
                    <button type="submit" class="transition duration-300 border hover:bg-gray-700 p-3">Create</button>
                </div>

            </form>
        </div>
    </x-slot>

</x-app-layout>
