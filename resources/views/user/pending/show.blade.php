<x-app-layout>
    <x-slot name="header">
        {{ $pending->hanzi }}
    </x-slot>

        <div class="max-w-7xl mx-auto text-left p-3 px-8 text-white">
            <table class="table-auto w-1/2 text-xl">
                <form action="{{ route('user.word.accept.pending', $pending->id) }}" method="POST">
                    @csrf
                    <tr>
                        <th>Hanzi</th>
                        <td class="p-3">
                            <a href="https://translate.google.com/?sl=zh-CN&tl=en&text={{ $pending->hanzi }}&op=translate" class="transition duration-300 text-purple hover:text-green" target="_blank" rel="noopener norefresh">
                                {{ $pending->hanzi }}
                            </a>
                        </td>
                    </tr>

                    <tr>
                        <th>Pinyin</th>
                        <td class="p-3">{{ $pending->pinyin }}</td>
                    </tr>

                    <tr>
                        <th>Translation</th>
                        <td class="p-3">{{ $pending->translation }}</td>
                    </tr>

                    <tr>
                        <th>Level</th>
                        <td class="p-3">
                            <select name="level" class="bg-transparent p-2 border w-3/4">
                                <option value="beginner">Beginner</option>
                                <option value="intermediate">Intermediate</option>
                                <option value="advanced">Advanced</option>
                            </select>
                        </td>
                    </tr>

                    <tr>
                        <th>Category</th>
                        <td class="p-3">
                            <select name="category" class="bg-transparent p-2 border w-3/4">
                                @foreach ($categories as $c)
                                    <option value="{{ $c->id }}">{{ $c->name }}</option>
                                @endforeach
                            </select>
                        </td>
                    </tr>

                    <tr>
                        <th></th>
                        <td class="p-3 pt-12">
                            <button type="submit" class="transition duration-300 hover:bg-gray-700 p-3 bg-transparent border">Save</button>
                        </td>
                    </tr>
                </form>
            </table>
        </div>
</x-app-layout>
