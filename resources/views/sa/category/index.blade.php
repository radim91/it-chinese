<x-app-layout>

    <x-slot name="header">
        <div class="grid grid-cols-2">
            <div>Categories
                @if (session('success'))
                    | {{ session('success') }}
                @endif</div>
            <div class="text-right">
                <a href="{{ route('sa.category.create') }}" class="transition duration-300 text-lg p-3 border hover:bg-gray-700">Create</a>
            </div>
        </div>

    </x-slot>

    <x-slot name="slot">
        <div class="max-w-7xl mx-auto py-12 px-4 text-white">
            <div class="pt-6 px-4">
                <table class="table-auto w-full">
                    <tr class="text-left">
                        <th>ID</th>
                        <th>Date</th>
                        <th>Name</th>
                        <th>Description</th>
                        <th class="text-right">Actions</th>
                    </tr>

                    @foreach ($categories as $c)
                        <tr>
                            <td>{{ $c->id }}</td>
                            <td>{{ $c->created_at }}</td>
                            <td>{{ $c->name }}</td>
                            <td>{{ $c->description }}</td>
                            <td class="text-right">
                                <a href="{{ route('sa.category.edit', $c->slug) }}" class="transition duration-300 text-yellow hover:green">Edit</a>
                                <a href="{{ route('sa.category.delete', $c->slug) }}" class="transition duration-300 text-red hover:text-purple">Delete</a>
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </x-slot>
</x-app-layout>

