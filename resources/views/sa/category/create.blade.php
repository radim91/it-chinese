<x-app-layout>
    <x-slot name="header">
        Create Category
        @if (session('error'))
            | {{ session('error') }}
        @endif
    </x-slot>

    <x-slot name="slot">
        <div class="max-w-7xl mx-auto px-4 py-12 text-white text-lg grid grid-rows-3">
            <form method="POST">
                @csrf

                <div class="px-6">
                    <label>
                        <p class="pb-4">Name</p>
                        <input type="text" name="name" id="name" class="@error('name') bg-red-200 @enderror bg-transparent border p-2 w-1/2" value="{{ old('name') }}">
                        @error('name') Name already in use. @enderror
                    </label>

                </div>

                <div class="pt-6 px-6">
                    <label>
                        <p class="pb-6">Description</p>
                        <textarea name="description" id="description" class="bg-transparent w-1/2 border p-2">{{ old('description') }}</textarea>
                        @error('description') Description too short @enderror
                    </label>

                </div>

                <div class="pt-6 px-6">
                    <button type="submit" class="transition duration-300 border hover:bg-gray-700 p-3">Create</button>
                </div>
            </form>
        </div>
    </x-slot>

</x-app-layout>
