<x-guest-layout>

        <div class="pt-1/6 text-center">
            <span class="text-4xl text-white">中文.tech</span>
        </div>

        <x-jet-validation-errors class="mb-4" />

        @if (session('status'))
            <div class="mb-4 font-medium text-sm text-green-600">
                {{ session('status') }}
            </div>
        @endif

        <div class="w-1/4 mx-auto pt-6 text-white">
            <form method="POST" action="{{ route('login') }}">
                @csrf

                <div>
                    <x-jet-label for="username" value="{{ __('Username') }}" />
                    <x-jet-input id="username" class="block mt-1 w-full bg-transparent border" type="username" name="username" :value="old('username')" required autofocus />
                </div>

                <div class="mt-4">
                    <x-jet-label for="password" value="{{ __('Password') }}" />
                    <x-jet-input id="password" class="block mt-1 w-full  bg-transparent border" type="password" name="password" required autocomplete="current-password" />
                </div>

                <div class="block mt-4">
                    <label for="remember_me" class="flex items-center">
                        <input id="remember_me" type="checkbox" class="form-checkbox" name="remember">
                        <span class="ml-2 text-sm text-gray-600">{{ __('Remember me') }}</span>
                    </label>
                </div>

                <div class="flex items-center justify-end mt-4">
                    <button class="transition duration-300 hover:bg-gray-700 ml-4 p-3 border">
                        {{ __('Login') }}
                    </button>
                </div>
            </form>
        </div>

</x-guest-layout>
