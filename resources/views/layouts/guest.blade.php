<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>中文.tech</title>

        <link rel="stylesheet" href="{{ mix('css/app.css') }}">
        <link rel="stylesheet" href="{{ asset('css/pretty-checkbox.min.css') }}">
        <script src="{{ mix('js/app.js') }}" defer></script>

        <!-- OG tags -->
        <meta property="og:url"                content="https://zhongwen.tech/" />
        <meta property="og:type"               content="website" />
        <meta property="og:title"              content="中文.tech" />
        <meta property="og:description"        content="Learn and review chinese tech vocabulary!" />
        <meta property="og:image"              content="https://zhongwen.tech/thumbnail.png" />

        <!-- Scripts -->
        <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.7.3/dist/alpine.js" defer></script>
    </head>
    <body>
        <div class="h-screen antialiased bg-gray-800" id="app">
            {{ $slot }}
        </div>
    </body>
</html>
