require('./bootstrap');
require('alpinejs');

import Vue from "vue";
import VueRouter from "vue-router";
import store from './store.js';
import router from './router.js';

Vue.use(VueRouter)

import App from './views/App';

const app = new Vue({
   el: "#app",
   components: { App },
   router,
   store,
});
