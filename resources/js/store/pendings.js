import axios from "axios";

export const namespaced = true;

export const state = {
    pending: Object,
}

export const mutations = {
    SET_PENDING(state, pending) {
        state.pending = pending;
    }
}

export const actions = {
    new({ commit }, pending) {
        return axios
            .post("/api/guest/add_word/", pending)
            .then(response => {
                commit('SET_PENDING', response.data);

                return response.data;
            })
            .catch(err => {
                console.log(err);
            });
    }
}
