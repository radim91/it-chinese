import axios from "axios";

export const namespaced = true;

export const state = {
    word: null,
    words: Array,
}

export const mutations = {
    SET_WORD(state, word) {
        state.word = word
    },
    SET_WORDS(state, words) {
        state.words = words
    },
    SET_LEVEL(state, level) {
        state.level = level
    }
}

export const actions = {
    randomWord({ commit }) {
        axios
            .get("/api/guest/random_word")
            .then(response => {
                commit("SET_WORD", response.data);
            })
            .catch(error => {
                console.log(error)
            })
    },
    new({ commit }, level) {
        axios
            .get("/api/guest/learn/new/"+level)
            .then(response => {
                commit("SET_WORDS", response.data);
            })
            .catch(error => {
                console.log(error)
            })
    },
    train({ commit }, level) {
        axios
            .get("/api/guest/learn/train/"+level)
            .then(response => {
                commit("SET_WORDS", response.data)
            })
            .catch(error => {
                console.log(error)
            })
    }
}
