import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter)

import Home from './views/components/Home';
import Learn from './views/components/Learn';
import Beginner from './views/components/Beginner';
import Intermediate from './views/components/Intermediate';
import Advanced from './views/components/Advanced';
import AddWord from "@/views/components/AddWord";

const router = new VueRouter({
    // mode: 'history',
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home
        },
        {
            path: '/learn',
            name: 'learn',
            component: Learn
        },
        {
            path: '/learn/beginner',
            name: 'beginner',
            component: Beginner
        },
        {
            path: '/learn/intermediate',
            name: 'intermediate',
            component: Intermediate
        },
        {
            path: '/learn/advanced',
            name: 'advanced',
            component: Advanced
        },
        {
            path: '/add-word',
            name: 'addword',
            component: AddWord
        }
    ]
});

export default router;
