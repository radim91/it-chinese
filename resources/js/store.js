import Vue from "vue";
import Vuex from "vuex";
import * as words from './store/words.js';
import * as pendings from './store/pendings.js';

Vue.use(Vuex);

export default new Vuex.Store({
   modules: {
       words,
       pendings
   }
});
