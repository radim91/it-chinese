<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware('guest')->prefix('guest')->name('guest.')->group(function () {
    Route::get('random_word', [HomeController::class, 'randomWord']);
    Route::post('add_word', [HomeController::class, 'addWord']);
    Route::get('learn/new/{level}', [HomeController::class, 'new']);
    Route::get('learn/train/{level}', [HomeController::class, 'train']);
});
