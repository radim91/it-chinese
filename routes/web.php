<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SA\CategoryController;
use App\Http\Controllers\User\WordController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\DashboardController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index'])->name('home.index');

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');

//SA
Route::middleware('sa')->name('sa.')->prefix('sa')->group(function() {
    //CATEGORY
    Route::get('/category', [CategoryController::class, 'index'])->name('category.index');
    Route::get('/category/create', [CategoryController::class, 'create'])->name('category.create');
    Route::post('/category/create', [CategoryController::class, 'store']);
    Route::get('/category/edit/{category:slug}', [CategoryController::class, 'edit'])->name('category.edit');
    Route::post('/category/edit/{category:slug}', [CategoryController::class, 'update']);
    Route::get('/category/delete/{category:slug}', [CategoryController::class, 'delete'])->name('category.delete');
});

//USER
Route::middleware(['auth:sanctum', 'verified'])->name('user.')->prefix('user')->group(function() {
   //WORDS
    Route::get('/word', [WordController::class, 'index'])->name('word.index');
    Route::get('/word/create', [WordController::class, 'create'])->name('word.create');
    Route::post('/word/create', [WordController::class, 'store']);
    Route::get('/word/edit/{word:id}', [WordController::class, 'edit'])->name('word.edit');
    Route::post('/word/edit/{word:id}', [WordController::class, 'update']);
    Route::get('/word/delete/{word:id}', [WordController::class, 'delete'])->name('word.delete');
   //PENDINGS
    Route::get('/word/pending/delete/{pending:id}', [WordController::class, 'deletePending'])->name('word.delete.pending');
    Route::get('/word/pending/show/{pending:id}', [WordController::class, 'showPending'])->name('word.show.pending');
    Route::post('/word/accept/{pending:id}', [WordController::class, 'acceptPending'])->name('word.accept.pending');
});
