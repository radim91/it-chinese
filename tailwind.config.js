const defaultTheme = require('tailwindcss/defaultTheme');

module.exports = {
    purge: [
        './vendor/laravel/jetstream/**/*.blade.php',
        './storage/framework/views/*.php',
        './resources/views/**/*.blade.php',
    ],

    theme: {
        extend: {
            fontFamily: {
                sans: ['Source Code Pro', ...defaultTheme.fontFamily.sans],
            },
            backgroundColor: ['checked'],
            borderColor: ['checked'],
        },
        textColor: {
            'white': '#d6d3cd',
            'red': '#e2595a',
            'yellow': '#f8cf60',
            'green': '#71cd21',
            'purple': '#a49ad6',
            'black': '#000000'
        }
    },

    variants: {
        opacity: ['responsive', 'hover', 'focus', 'disabled'],
    },

    plugins: [
        require('@tailwindcss/ui'),
    ],
};
