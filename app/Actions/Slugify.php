<?php

namespace App\Actions;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class Slugify {

    public static function new(string $name): string {
        return Str::slug($name, '-');
    }

    public static function exists(string $table, string $slug): bool {
        if (DB::table($table)->where('slug', $slug)->count() > 0) {
            return true;
        } else {
            return false;
        }
    }
}
