<?php

namespace App\Actions\Category;

use App\Actions\Validation;
use Illuminate\Http\Request;

class CategoryValidation extends Validation {

    const CREATE_RULES = [
      'name' => 'required|max:255|unique:categories',
      'description' => 'required|min:10',
    ];

    const EDIT_RULES = [
        'name' => 'required|max:255',
        'description' => 'required|min:10',
    ];

    public static function isValid(Request $request): bool {
        return $request->validate(self::CREATE_RULES) ? true : false;
    }

    public static function isValidEdit(Request $request): bool {
        return $request->validate(self::EDIT_RULES) ? true : false;
    }
}
