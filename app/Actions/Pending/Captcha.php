<?php

namespace App\Actions\Pending;

class Captcha {
    public static function check(string $token): bool {
        $secret = '6Ld-oxoaAAAAAGWYO9_Z_FY7s5ppyMLEHQt49Ltl';
        $url = "https://www.google.com/recaptcha/api/siteverify?secret=$secret&response=".$token;
        $verify = json_decode(file_get_contents($url));

        return !$verify->success ? false : true;
    }
}
