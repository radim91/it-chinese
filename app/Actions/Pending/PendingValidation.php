<?php

namespace App\Actions\Pending;

use App\Actions\Validation;
use Illuminate\Http\Request;

class PendingValidation extends Validation {

    private const RULES = [
      'hanzi' => 'required|max:255',
      'pinyin' => 'required|max:255',
      'translation' => 'required|max:255',
      'level' => 'required|max:13'
    ];

    public static function isValid(Request $request): bool {
        return $request->validate(self::RULES) ? true : false;
    }
}
