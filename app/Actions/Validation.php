<?php

namespace App\Actions;

use Illuminate\Http\Request;

abstract class Validation {
    abstract public static function isValid(Request $request): bool;
}
