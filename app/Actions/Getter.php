<?php

namespace App\Actions;

use Illuminate\Pagination\LengthAwarePaginator;

abstract class Getter {
    abstract public static function paginate(string $key, string $order, int $count): LengthAwarePaginator;
}
