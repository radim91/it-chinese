<?php

namespace App\Actions\Word;

use App\Actions\Validation;
use Illuminate\Http\Request;

class WordValidation extends Validation {

    const RULES = [
        'hanzi' => 'required',
        'pinyin' => 'required|max:255',
        'translation' => 'required',
        'category' => 'required|integer',
        'level' => 'required|string'
    ];

    public static function isValid(Request $request): bool {
        return $request->validate(self::RULES) ? true : false;
    }
}
