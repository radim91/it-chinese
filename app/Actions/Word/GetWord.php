<?php

namespace App\Actions\Word;


use App\Actions\Getter;
use App\Models\Word;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;

class GetWord extends Getter {

    public static function paginate(string $key, string $order, int $count): LengthAwarePaginator {
        return DB::table('words')->orderBy($key, $order)->paginate($count);
    }

    public static function mixTranslations(string $level): array {
        $words = [];

        $dbSelection = DB::table('words')
            ->where('level', $level)
            ->inRandomOrder()
            ->limit(10)
            ->get();

        foreach ($dbSelection as $db) {
            $randomTrans = Word::all()->random(2);
            $words[$db->id] = [
                'hanzi' => $db->hanzi,
                'pinyin' => $db->pinyin,
                'translation' => $db->translation,
                'option1' => $randomTrans[0]->translation,
                'option2' => $randomTrans[1]->translation
            ];
        }

        return $words;
    }
}
