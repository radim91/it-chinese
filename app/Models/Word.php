<?php

namespace App\Models;

use App\Actions\Word\GetWord;
use App\Actions\Word\WordValidation;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Pending;

/**
 * @method static create()
 * @method static where(string $string, string $level)
 */

class Word extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function categories() {
        return $this->belongsTo(Category::class, 'category');
    }

    public function new(Request $request): bool {
        if (WordValidation::isValid($request)) {
            self::create($request->all());
            return true;
        } else {
            return false;
        }
    }

    public function change(Word $word, Request $request): bool {
        if (WordValidation::isValid($request)) {
            $word->update($request->all());
            $word->save();
            return true;
        } else {
            return false;
        }
    }

    public function random(): Collection {
        $words = self::all();
        return $words->random(1);
    }

    public function learnNew(string $level): Collection {
        return DB::table('words')
            ->where('level', $level)
            ->inRandomOrder()
            ->limit(10)
            ->get();
    }

    public function train(string $level): array {
        return GetWord::mixTranslations($level);
    }

    public function accept(Request $request, Pending $pending): bool {
        $mergedData = array_merge($request->all(), $pending->attributesToArray());

        if($mergedData != null) {
            self::create($mergedData);
            $pending->delete();
            return true;
        } else {
            return false;
        }
    }
}
