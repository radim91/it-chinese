<?php

namespace App\Models;

use App\Actions\Pending\Captcha;
use App\Actions\Pending\PendingValidation;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Models\Word;

class Pending extends Model {
    use HasFactory;

    protected $guarded = ['id'];

    public function new(Request $request): bool {
        if (PendingValidation::isValid($request) && Captcha::check($request->recaptcha)) {
            self::create($request->all());
            return true;
        } else {
            return false;
        }
    }
}
