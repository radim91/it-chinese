<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Actions\Category\CategoryValidation;
use App\Actions\Slugify;

/**
 * @method static create(array $all)
 */

class Category extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function words() {
        return $this->hasMany(Word::class);
    }

    public function new(Request $request): bool {
        $slug = Slugify::new($request->name);
        if (CategoryValidation::isValid($request)) {
            $merged = array_merge($request->all(), ['slug' => $slug]);
            Category::create($merged);
            return true;
        } else {
            return false;
        }
    }

    public function change(Category $category, Request $request): bool {
        $slug = Slugify::new($request->name);
        if (CategoryValidation::isValidEdit($request)) {
            $merged = array_merge($request->all(), ['slug' => $slug]);
            $category->update($merged);
            $category->save();

            return true;
        } else {
            return false;
        }
    }
}
