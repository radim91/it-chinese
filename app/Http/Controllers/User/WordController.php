<?php

namespace App\Http\Controllers\User;

use App\Actions\Word\GetWord;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Pending;
use App\Models\Word;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class WordController extends Controller {

    public function __construct(Word $word) {
        $this->word = $word;
    }

    public function index(): View {
        $words = GetWord::paginate('created_at', 'DESC', 15);
        return view('user/word/index', [
            'words' => $words
        ]);
    }

    public function create(): View {
        $categories = Category::all();
        return view('user/word/create', [
            'categories' => $categories
        ]);
    }

    public function store(Request $request): RedirectResponse {
        if ($this->word->new($request)) {
            return redirect()->route('user.word.index')->with('success', 'Word successfully created.');
        } else {
            return back()->with('error', 'Word creation failed.');
        }
    }

    public function edit(Word $word): View {
        $categories = Category::all();
        return view('user/word/edit', [
           'word' => $word,
           'categories' => $categories
        ]);
    }

    public function update(Word $word, Request $request): RedirectResponse {
        if ($this->word->change($word, $request)) {
            return redirect()->route('user.word.index')->with('success', 'Word successfully edited.');
        } else {
            return back()->with('error', 'Edit failed.');
        }
    }

    public function delete(Word $word): RedirectResponse {
        try {
            $word->delete();
        } catch (\Exception $e) {
            echo $e;
        }

        return redirect()->route('user.word.index')->with('success', 'Word successfully deleted.');
    }

    public function acceptPending(Request $request, Pending $pending): RedirectResponse {
        if ($this->word->accept($request, $pending)) {
            return redirect()->route('dashboard')->with('success', 'Pending word accepted successfully!');
        } else {
            return back()->with('error', "Pending word wasn't accepted unfortunately.");
        }
    }

    public function deletePending(Pending $pending): RedirectResponse {
        try {
            $pending->delete();
        } catch (\Exception $e) {
            echo $e;
        }

        return back()->with('success', 'Pending successfully deleted.');
    }

    public function showPending(Pending $pending): View {
        $categories = Category::all();
        return view('/user/pending/show', compact('pending', 'categories'));
    }

}
