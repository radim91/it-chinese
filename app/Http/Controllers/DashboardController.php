<?php

namespace App\Http\Controllers;

use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Illuminate\Support\Facades\DB;
use App\Models\Pending;
use App\Models\Word;

class DashboardController extends Controller
{
    public function index(): View {
        $pendings = DB::table('pendings')->orderByDesc('created_at')->paginate(10);
        return view('dashboard', compact('pendings'));
    }
}
