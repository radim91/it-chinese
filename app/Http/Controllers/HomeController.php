<?php

namespace App\Http\Controllers;

use http\Env\Response;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Models\Word;
use App\Models\Pending;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class HomeController extends Controller {

    private Word $word;
    private Pending $pending;

    public function __construct(Word $word, Pending $pending) {
        $this->word = $word;
        $this->pending = $pending;
    }

    public function index(): View {
        $auth = Auth::user();
        return view('index', [
            'auth' => $auth
        ]);
    }

    public function randomWord(): JsonResponse {
        $word = $this->word->random()[0];
        return response()->json($word);
    }

    public function addWord(Request $request): JsonResponse {
        return $this->pending->new($request) ? response()->json(['status' => 'ok']) : response()->json(['status' => 'fail']);
    }

    public function new(string $level): JsonResponse {
        $words = $this->word->learnNew($level);
        return response()->json($words);
    }

    public function train(string $level): JsonResponse {
        $words = $this->word->train($level);
        return response()->json($words);
    }
}
