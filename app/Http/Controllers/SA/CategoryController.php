<?php

namespace App\Http\Controllers\SA;

use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;
use App\Models\Category;

class CategoryController extends Controller {

    private $category;

    public function __construct(Category $category) {
        $this->category = $category;
    }

    public function index(): View {
        $categories = DB::table('categories')->orderByDesc('created_at')->get();
        return view('sa/category/index', compact('categories'));
    }

    public function create(): View {
        return view('sa/category/create');
    }

    public function store(Request $request): RedirectResponse {
        if ($this->category->new($request)) {
            return redirect()->route('sa.category.index')->with('success', 'Category was created successfully.');
        } else {
            return back()->with('error', 'Category creation failed.');
        }
    }

    public function edit(Category $category): View {
        return view('sa/category/edit', [
           'category' => $category
        ]);
    }

    public function update(Category $category, Request $request): RedirectResponse {
        if ($this->category->change($category, $request)) {
            return redirect()->route('sa.category.index')->with('success', 'Category was edited successfully.');
        } else {
            return back()->with('error', 'Category edit failed.');
        }
    }

    public function delete(Category $category): RedirectResponse {
        try {
            $category->delete();
        } catch (\Exception $e) {
            echo $e;
        }

        return redirect()->route('sa.category.index')->with('success', 'Category was successfully deleted.');
    }
}
